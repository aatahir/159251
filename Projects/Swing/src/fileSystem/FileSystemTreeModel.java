package fileSystem;
import java.io.File;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class FileSystemTreeModel implements TreeModel{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object getRoot() {
		return new File(".");
	}

	@Override
	public Object getChild(Object parent, int index) {
		File file = (File)parent;
		return file.listFiles()[index];

	}

	@Override
	public int getChildCount(Object parent) {
		File file = (File)parent;
		return file.listFiles().length;

	}

	@Override
	public boolean isLeaf(Object node) {
		File file = (File)node;
		return !file.isDirectory();

	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
		// TODO Auto-generated method stub
		
	}

}
